# Contributor: nibon7 <nibon7@163.com>
# Maintainer: nibon7 <nibon7@163.com>
pkgname=nushell
pkgver=0.83.1
pkgrel=0
pkgdesc="A new type of shell"
url="https://www.nushell.sh"
# s390x: nix crate
arch="all !s390x"
license="MIT"
makedepends="
	cargo
	cargo-auditable
	libgit2-dev
	openssl-dev
	sqlite-dev
	"
checkdepends="bash"
subpackages="$pkgname-plugins:_plugins"
install="$pkgname.post-install $pkgname.post-upgrade $pkgname.pre-deinstall"
source="$pkgname-$pkgver.tar.gz::https://github.com/nushell/nushell/archive/$pkgver.tar.gz
	system-deps.patch
	"

case "$CARCH" in
# ooms when building
armhf|armv7|ppc64le|riscv64|x86) _exclude_opts="--exclude nu-cmd-dataframe" ;;
esac

prepare() {
	default_prepare

	# Rust target triple.
	local target=$(rustc -vV | sed -n 's/host: //p')

	# Build against system-provided libs
	mkdir -p .cargo
	cat >> .cargo/config.toml <<-EOF
		[target.$target]
		git2 = { rustc-link-lib = ["git2"] }
		rusqlite = { rustc-link-lib = ["sqlite3"] }
	EOF

	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --workspace --release --frozen $_exclude_opts
}

check() {
	cargo test --workspace --frozen $_exclude_opts
}

package() {
	find target/release \
		-maxdepth 1 \
		-executable \
		-type f \
		-name "nu*" \
		-exec install -Dm755 '{}' -t "$pkgdir"/usr/bin/ \;
}

_plugins() {
	pkgdesc="Nushell plugins"
	depends="nushell=$pkgver-r$pkgrel"

	amove usr/bin/nu_plugin_*
}

sha512sums="
b7968e702af382aa00e588dc5f181c9edcd9f01fe2bfdccebc964d6c00d8320d96b59f34d57cc6733c4c5226b69fb9e5026fb0f47948170957c04b7eb1eef482  nushell-0.83.1.tar.gz
38da14ab1dfb0f4c8261c8578f6a3cd47c7a6cf14d948117980b0e896ba7ee37c27ed230fe8ed1717e0ff24e0cf763dd4e54ed7c38f34ca946b07725da2792d4  system-deps.patch
"
