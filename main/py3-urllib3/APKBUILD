# Contributor: Yura Kushnir <kushnir.yura@gmail.com>
# Maintainer: Francesco Colista <fcolista@alpinelinux.org>
pkgname=py3-urllib3
_pkgname=urllib3
pkgver=1.26.16
pkgrel=0
pkgdesc="HTTP library with thread-safe connection pooling, file post, and more"
url="https://github.com/urllib3/urllib3"
arch="noarch"
license="MIT"
depends="python3"
makedepends="
	py3-gpep517
	py3-setuptools
	py3-wheel
	"
subpackages="$pkgname-pyc"
source="https://github.com/urllib3/urllib3/releases/download/$pkgver/urllib3-$pkgver.tar.gz"
builddir="$srcdir/$_pkgname-$pkgver"
options="!check" # needs py3-tornado from community

replaces="py-urllib3" # Backwards compatibility
provides="py-urllib3=$pkgver-r$pkgrel" # Backwards compatibility

# secfixes:
#   1.25.9-r0:
#     - CVE-2020-26137
#   1.26.4-r0:
#     - CVE-2021-28363

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
caa58af5bf49644459a9074be659511b56c5d66802153c72ba2ba2eb9c0aafd56b4cd8f8588964ee7d2a9b16a5e114d62298ab1e128710260ed86f1e315b5571  urllib3-1.26.16.tar.gz
"
