# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Maintainer: Jakub Jirutka <jakub@jirutka.cz>
pkgname=nghttp3
pkgver=0.14.0
pkgrel=0
pkgdesc="HTTP/3 library written in C"
url="https://github.com/ngtcp2/nghttp3"
arch="all"
license="MIT"
makedepends="cmake samurai"
checkdepends="cunit-dev"
subpackages="$pkgname-dev"
source="https://github.com/ngtcp2/nghttp3/releases/download/v$pkgver/nghttp3-$pkgver.tar.gz"

build() {
	local crossopts=
	if [ "$CBUILD" != "$CHOST" ]; then
		crossopts="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake -G Ninja -B build \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		$crossopts .
	cmake --build build
}

check() {
	cmake --build build --target check
}

package() {
	DESTDIR="$pkgdir" cmake --install build

	# Contains just README.rst.
	rm -rf "$pkgdir"/usr/share/doc
}

sha512sums="
8f7f9b3860bd3cc8f6bdc479d40b44784a1fd6f21644cd72f311d5f506cf58e3539c7d977e7c5bc436c54ce5010eb9bfe4de7e946f89cb1a1cb4f31577a2f0df  nghttp3-0.14.0.tar.gz
"
