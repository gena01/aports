# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-plasma
pkgname=powerdevil
pkgver=5.27.7
pkgrel=1
pkgdesc="Manages the power consumption settings of a Plasma Shell"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/plasma-desktop/"
license="GPL-2.0-or-later AND LGPL-2.0-or-later AND (LGPL-2.1-only OR LGPL-3.0-only)"
depends="upower"
depends_dev="
	bluez-qt-dev
	ddcutil-dev
	eudev-dev
	kactivities-dev
	kauth-dev
	kconfig-dev
	kcrash-dev
	kdbusaddons-dev
	kglobalaccel-dev
	ki18n-dev
	kidletime-dev
	kio-dev
	knotifications-dev
	knotifyconfig-dev
	kwayland-dev
	libkscreen-dev
	networkmanager-qt-dev
	plasma-workspace-dev
	qt5-qtbase-dev
	qt5-qtx11extras-dev
	solid-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	kdoctools-dev
	libcap-utils
	samurai
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
_repo_url="https://invent.kde.org/plasma/powerdevil.git"
source="https://download.kde.org/$_rel/plasma/$pkgver/powerdevil-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-libs $pkgname-doc $pkgname-lang"

_commit=""
snapshot() {
	clean
	makedepends="git xz rsync tar" deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone --filter=tree:0 https://invent.kde.org/plasma/powerdevil.git .
	git archive --format tar --prefix=$pkgname/ $_commit > ../$pkgname-$_commit.tar
	xz -vv -T0 -9 -e ../$pkgname-$_commit.tar
}

build() {
	# HAVE_DDCUTIL needs to be set manually for now but is enabled by default in the next
	# (post 5.27.3) powerdevil version. Nothing changed in particular warranting it to be
	# enabled again, but upstream mentions the reasons for originally disabling it by default
	# aren't known anymore and it's time to re-enable again
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DHAVE_DDCUTIL=On
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build

	# We don't ship systemd
	rm -r "$pkgdir"/usr/lib/systemd

	# org_kde_powerdevil has CAP_WAKE_ALARM set and this breaks dbus
	# Remove CAP_WAKE_ALARM from org_kde_powerdevil to make it work again
	setcap -r "$pkgdir"/usr/lib/libexec/org_kde_powerdevil
}

sha512sums="
0df1028b3fe44d017255d6fc517d4ff8b9602b758c1535c066a07c2d1add37d45ad2d9067b0fecc4f705afea6b064fcb8eb5c6421704f58b391e68a56fd14bd5  powerdevil-5.27.7.tar.xz
"
