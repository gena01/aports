# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=containers-common
pkgver=0.55.2
pkgrel=0
pkgdesc="Configuration files for container tools"
url="https://github.com/containers/common"
license="Apache-2.0"
arch="noarch"
options="!check" # no test suite
makedepends="go-md2man"
subpackages="$pkgname-doc"
# Pick the exact versions of common/storage/image vendored in podman.
# Ideally, they should be the same in skopeo and buildah.
# Check them with the list_vendors function.
_common_ver=$pkgver
_storage_ver=1.48.0
_image_ver=5.26.1
_podman_ver=4.6.0
_skopeo_ver=1.13.1
_buildah_ver=1.31.1
_shortnames_ver=2023.02.20
source="https://github.com/containers/common/archive/v$_common_ver/common-$_common_ver.tar.gz
	https://github.com/containers/storage/archive/v$_storage_ver/storage-$_storage_ver.tar.gz
	https://github.com/containers/image/archive/v$_image_ver/image-$_image_ver.tar.gz
	https://github.com/containers/podman/archive/v$_podman_ver/podman-$_podman_ver.tar.gz
	https://github.com/containers/skopeo/archive/v$_skopeo_ver/skopeo-$_skopeo_ver.tar.gz
	https://github.com/containers/buildah/archive/v$_buildah_ver/buildah-$_buildah_ver.tar.gz
	https://github.com/containers/shortnames/archive/v$_shortnames_ver/shortnames-$_shortnames_ver.tar.gz
	"

list_vendors() {
	unpack

	for tool in podman-$_podman_ver skopeo-$_skopeo_ver buildah-$_buildah_ver; do
		cd "$srcdir"/$tool
		msg $tool
		grep github.com/containers/common go.mod
		grep github.com/containers/storage go.mod
		grep github.com/containers/image go.mod
	done
}

prepare() {
	default_prepare

	# fix go-md2man path in containers/storage
	sed -E 's/(GOMD2MAN =).*/\1 go-md2man/' -i "$srcdir"/storage-$_storage_ver/docs/Makefile

	# set default storage driver
	sed -E 's/(driver =) ""/\1 "overlay"/' -i "$srcdir"/storage-$_storage_ver/storage.conf

	# set unqualified-search-registries
	sed -E 's/# (unqualified-search-registries =).*/\1 ["docker.io"]/' -i "$srcdir"/image-$_image_ver/registries.conf
}

build() {
	cd "$srcdir"/common-$_common_ver
	make -C docs

	cd "$srcdir"/storage-$_storage_ver
	make -C docs

	cd "$srcdir"/image-$_image_ver
	make docs
}

package() {
	install -d "$pkgdir"/etc/containers/certs.d
	install -d "$pkgdir"/etc/containers/oci/hooks.d
	install -d "$pkgdir"/var/lib/containers/sigstore

	cd "$srcdir"/common-$_common_ver
	install -Dm644 pkg/config/containers.conf "$pkgdir"/etc/containers/containers.conf
	install -Dm644 pkg/config/containers.conf "$pkgdir"/usr/share/containers/containers.conf
	install -Dm644 pkg/seccomp/seccomp.json "$pkgdir"/etc/containers/seccomp.json
	install -Dm644 pkg/seccomp/seccomp.json "$pkgdir"/usr/share/containers/seccomp.json
	make -C docs install PREFIX=/usr DESTDIR="$pkgdir"

	cd "$srcdir"/storage-$_storage_ver
	install -Dm644 storage.conf "$pkgdir"/etc/containers/storage.conf
	install -Dm644 storage.conf "$pkgdir"/usr/share/containers/storage.conf
	make -C docs install DESTDIR="$pkgdir"

	cd "$srcdir"/image-$_image_ver
	install -Dm644 registries.conf "$pkgdir"/etc/containers/registries.conf
	make install DESTDIR="$pkgdir"

	cd "$srcdir"/skopeo-$_skopeo_ver
	install -Dm644 default-policy.json "$pkgdir"/etc/containers/policy.json
	install -Dm644 default.yaml "$pkgdir"/etc/containers/registries.d/default.yaml

	cd "$srcdir"/shortnames-$_shortnames_ver
	install -Dm644 shortnames.conf "$pkgdir"/etc/containers/registries.conf.d/00-shortnames.conf
}

doc() {
	default_doc
	pkgdesc="Man pages for container tools"
}

sha512sums="
0459dd83bd0fab9843b9c5ac413ab068f1ceadd4c0dc2c23c1d58c20679e94b80b049ae2e14359d7d9ae0976cb2cb513cce4ccbc5e9a0598ccfb7fe65ab07163  common-0.55.2.tar.gz
30c26b40f2858622ce4f63d21cee3ff626ac17f57ddcaf23a2d2d16b785259587cb7eda13309dc145860c3dd847b8546863eb0a32e63609586857c63e44b638a  storage-1.48.0.tar.gz
fa10e98acca566586add698ab9f269da0d2eff72480dd9dc027b0b39b66df7b7e0799ab76ce17e9a93f2117ef124226d48fd56cffb124d8a5f89b883f61b60cb  image-5.26.1.tar.gz
a0f045e3275f072abcf2e59636527f1b98b7579d7b64df37cbabbf1f9d2cb3852a95240f6145ccd81d4b31da0175003ab0043d269c75f9dfb31586065554c986  podman-4.6.0.tar.gz
df9c3dc450707c91e3bed8816a90176292198779f59ad9dfbc4fa340a2d812e7761937b5e130a0bd4778c06b6670e9a081850fcb2b01f09e236f95cbedea6e06  skopeo-1.13.1.tar.gz
7375877d964197d0690542e1da0636b0a67cdf01f30ddbdd69cced5cfe4f8bb370b37ec58f2dffd2c2f048b897470d8fb06cd9f70c8e75df2aa6b19f86610f7b  buildah-1.31.1.tar.gz
856dbbeb2acda276e9605bd1ecec0f8d65952c597ee2af61dd8909d7d3c04e5ef06c40b69ec4a98f79e623c536850f614c1b0af3a19637e300e7d3a285933193  shortnames-2023.02.20.tar.gz
"
