# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=libkexiv2
pkgver=23.04.3
pkgrel=2
pkgdesc="Library to manipulate picture metadata"
url="https://www.kde.org/applications/graphics"
arch="all !armhf" # extra-cmake-modules
license="GPL-2.0-or-later"
makedepends="
	exiv2-dev
	extra-cmake-modules
	qt5-qtbase-dev
	samurai
	"
subpackages="$pkgname-dev"
_repo_url="https://invent.kde.org/graphics/libkexiv2.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/libkexiv2-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
7f41e2db52a7fdd9d08ed46fe88f97f7705c04f38adc983444ddbc6ff2b572c2462930870a27ebeb92a2fbe7ffb23a6e061c18ce07e781f468bb45ae5dffe5f0  libkexiv2-23.04.3.tar.xz
"
