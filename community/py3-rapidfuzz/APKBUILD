# Maintainer: Duncan Bellamy <dunk@denkimushi.com>
pkgname=py3-rapidfuzz
pkgver=3.2.0
pkgrel=0
pkgdesc="Rapid fuzzy string matching in Python using various string metrics"
url="https://github.com/maxbachmann/RapidFuzz"
arch="all"
license="MIT"
makedepends="
	cmake
	cython
	py3-gpep517
	py3-rapidfuzz-capi
	py3-scikit-build
	py3-setuptools
	python3-dev
	samurai
	"
checkdepends="
	py3-hypothesis
	py3-numpy
	pytest
	"
subpackages="$pkgname-pyc"
source="https://files.pythonhosted.org/packages/source/r/rapidfuzz/rapidfuzz-$pkgver.tar.gz"
builddir="$srcdir/rapidfuzz-$pkgver"

case "$CARCH" in
x86*)
	# float rounding
	options="$options !check"
	;;
esac

build() {
	RAPIDFUZZ_BUILD_EXTENSION=1 \
	CFLAGS="$CFLAGS -O2" \
	CXXFLAGS="$CXXFLAGS -O2 -U_FORTIFY_SOURCE" \
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer \
		.dist/rapidfuzz*.whl
	.testenv/bin/python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/rapidfuzz*.whl
}

sha512sums="
0ea0ecc62e2493519a302edd090521ed7efeae35e73812aeadaca2e2369362b57b1a095a09296edfa8db07b6bf58de4451dc71e96e6c215ebbe96dc1f8e7e995  rapidfuzz-3.2.0.tar.gz
"
